/*
 Gulpfile for theming Mendix (8+) themes.
 version 3.1.0
 Source: https://bitbucket.org/incentro-ondemand/atlas-ui-framework

 Build upon Mendix Atlas UI version 2.3.1
 Source: https://github.com/mendix/Atlas-UI-Framework
*/

'use strict';

import autoprefixer from 'gulp-autoprefixer';
import babel from 'gulp-babel';
import browserSync from 'browser-sync';
import cleanCSS from 'gulp-clean-css';
import concat from 'gulp-concat';
import debug from 'gulp-debug';
import del from 'del';
import gulp from 'gulp';
import sass from 'gulp-sass';
import strip from 'gulp-strip-comments';
import uglify from 'gulp-uglify';
import umd from 'gulp-umd';

const server = browserSync.create();
const proxyAddress = 'localhost:8080';
const paths = {
  styles: {
    src: 'theme/styles/web/sass/**/*.scss',
    dest: 'theme/styles/web/css',
    deployment: 'deployment/web/styles/web/css/'
  },
  scripts: {
    src: 'src/js/components/*.js',
    libs: 'src/js/libs/*.js',
    dest: 'theme/js/',
    deployment: 'deployment/web/js/'
  },
  files: {
    src: 'src/**/*.html',
    dest: 'theme/',
    deployment: 'deployment/web/'
  },
  fonts: {
    src: 'src/fonts/*',
    dest: 'theme/styles/web/css/fonts/',
    deployment: 'deployment/web/styles/web/css/fonts/'
  }
};

/**************************************************************************
 * Don't try to edit below this line, unless you know what you are doing *
 **************************************************************************/

// reload browser
function reload(done) {
  server.reload();
  done();
}

// init the browsersync
function serve(done) {
  server.init({
    proxy: proxyAddress,
    online: true,
    ws: true,
    open: false,
    notify: true
  });
  done();
}

// clear dest files/folders
const clean = () =>
  del([
    paths.styles.dest + '**',
    '!' + paths.styles.dest,
    paths.scripts.dest + '**',
    '!' + paths.scripts.dest
  ]);

// JavaScript files task
function scripts() {
  return gulp
    .src(paths.scripts.src)
    .pipe(concat('MxJS.js'))
    .pipe(babel({ presets: ['@babel/preset-env'] }))
    .pipe(umd({ templateName: 'amdNodeWeb' }))
    .pipe(strip())
    .pipe(uglify())
    .pipe(gulp.dest(paths.scripts.dest))
    .pipe(gulp.dest(paths.scripts.deployment))
    .pipe(debug({ title: '-- Build JS: ' }));
}

// JavaScript lib folder task
function vendorScripts() {
  return gulp
    .src(paths.scripts.libs)
    .pipe(strip())
    .pipe(uglify())
    .pipe(gulp.dest(paths.scripts.dest))
    .pipe(gulp.dest(paths.scripts.deployment))
    .pipe(debug({ title: '-- Moved lib JS: ' }));
}

// compile SASS
function styles() {
  return gulp
    .src(paths.styles.src)
    .pipe(sass({ outputStyle: 'expanded' }).on('error', sass.logError))
    .pipe(autoprefixer({ cascade: false }))
    .pipe(cleanCSS())
    .pipe(gulp.dest(paths.styles.dest))
    .pipe(gulp.dest(paths.styles.deployment))
    .pipe(debug({ title: '-- Build CSS: ' }));
}

// Include fonts
function fontsInclude() {
  return gulp
    .src(paths.fonts.src)
    .pipe(gulp.dest(paths.fonts.dest))
    .pipe(gulp.dest(paths.fonts.deployment))
    .pipe(debug({ title: '-- Fonts include: ' }));
}

// Watch for changes
const watch = () => {
  gulp.watch(paths.scripts.src, gulp.series(scripts, reload));
  gulp.watch(paths.styles.src, gulp.series(styles, reload));
  gulp.watch(paths.scripts.libs, gulp.series(vendorScripts, reload));
};

// Define 'dev' task
const dev = gulp.series(
  clean,
  gulp.parallel(scripts, styles),
  vendorScripts,
  serve,
  watch
);

gulp.task(
  'dev',
  gulp.series(
    clean,
    gulp.parallel(scripts, styles),
    vendorScripts,
    serve,
    watch
  )
);

// Make the 'dev' task the default for typing just 'gulp'
export default dev;
